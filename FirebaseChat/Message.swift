//
//  Message.swift
//  FirebaseChat
//
//  Created by Manish Manandhar on 2/10/17.
//  Copyright © 2017 Manish Manandhar. All rights reserved.
//

import UIKit
import Firebase

class Message: NSObject {
    var fromId: String?
    var text: String?
    var timestamp: NSNumber?
    var toId: String?
    
    func chatPartnerId() -> String? {
        ///returns toId if (fromId==current user id) is logged in, else return the fromId
        return fromId == FIRAuth.auth()?.currentUser?.uid ? toId : fromId
    }
}
