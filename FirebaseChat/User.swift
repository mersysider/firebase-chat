//
//  User.swift
//  FirebaseChat
//
//  Created by Manish Manandhar on 2/7/17.
//  Copyright © 2017 Manish Manandhar. All rights reserved.
//

import UIKit

class User: NSObject {
    var name: String?
    var email: String?
    var profileImageUrl: String?
    var id: String?
}
